@extends('layouts.app')

@section('content')

<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="adminlte/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="adminlte/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="adminlte/Ionicons/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="adminlte/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="adminlte/dist/css/AdminLTE.min.css">

<link rel="stylesheet" href="adminlte/dist/css/skins/_all-skins.min.css">

<link rel="stylesheet"
href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">


  <div class="col-md-6">
    <div class="box">
      <div class="box-header">
        <h1   class="box-title"><a href="home"><td><button  type="button" class="btn btn-block btn-primary btn-sm">Regresar a la pagina principal</button></td></a></h1>
      </div>

        <form method="POST" action="/RECARGAS_13-06/public/recargauff" role="form">                

         {{ csrf_field() }}
         @if(count($errors) > 0)
         <div class="alert alert-danger">

           <ul>
            @foreach($errors->all() as $error)
            <li>{{ $error}} </li>
            @endforeach
          </ul>
        </div>
        @endif

        @if (session()->has('flash'))
        <div class="">
          <div class="alert alert-success">{{ session('flash')}}</div>


        </div>
        @endif


        
        <div class="form-group">
          <a href=""><img src="operadores/opeuff.png"></a>

          <input type="hidden" name="idUsuario" value="21">
          <input type="hidden" name="idPago" value="1001">

          <input type="hidden" name=" idOperador" value="2">
        </div>


          <div class="form-group">
            <label for="">Valor a recargar</label>
            <input class="form-control"  name='valorRecarga' placeholder="$" type="number"  ue="{{ old('valorRecarga')}}" >
          </div>
          <div class="form-group">
            <label for="">Numero celular</label>
            <input class="form-control"  name='NumeroCelular' placeholder="Numero celular" type="number"  ue="{{ old('NumeroCelular')}}">
          </div>
          <div>
            <td>
              <button type="submit" class="btn btn-block btn-primary btn-sm">Realizar recarga</button>
            </td>
          </div>                 
        </form>  
      </div>
    </div>

     <div class="col-md-6">
          <div class="box box-solid">
             <!-- /.box-header -->
            <div class="box-body">
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
               
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="operadores/claroBaner1.jpeg" >

                   
                  </div>
                  <div class="item">
                    <img src="operadores/claroBaner2.jpeg" >
                   
                  </div>
                  
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                  <span class="fa fa-angle-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                  <span class="fa fa-angle-right"></span>
                </a>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
            <!-- /.box -->
          <section class="content">
      <div class="row">
        <div class="col-xs-12">
           <div class="box">
            <div class="box-header">
              <h3 class="box-title">Ultimas 5 recargas</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <?php

              if( count($recargas) >0){
                          ?>


              <table id="tabla_recargas" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Fecha</th>
                  <th>Numero</th>
                  <th>Costo</th>
                  <th>Saldo</th>
                  <th>Estado</th>
                </tr>
                </thead>

                
                <tbody>

                  <?php

                foreach ($recargas as $recarga) {
                 # code...
                ?>
                <tr>
                  <td><?= $recarga->created_at; ?></td>
                  <td><?= $recarga->NumeroCelular; ?>
                  </td>
                  <td><?= $recarga->valorRecarga; ?></td>
                  <td>pendiente</td>
                  <td>pendiente</td>
                </tr>
                <?php
              }
              ?>
                
                </tbody>
               
              </table>

              <?php
             
            }
            else
            {
              ?>
            <

            <?php
          }
          ?>




            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>  
            <!-- /.box-body -->
          </div>
        </div>



  




        @endsection
