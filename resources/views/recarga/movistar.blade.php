@extends('layouts.app')

@section('content')

<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="adminlte/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="adminlte/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="adminlte/Ionicons/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="adminlte/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="adminlte/dist/css/AdminLTE.min.css">

<link rel="stylesheet" href="adminlte/dist/css/skins/_all-skins.min.css">

<link rel="stylesheet"
href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">


  <div class="col-md-6">
    <div class="box">
      <div class="box-header">
        <h1   class="box-title"><a href="home"><td><button  type="button" class="btn btn-block btn-primary btn-sm">Regresar a la pagina principal</button></td></a></h1>
      </div>

        <form method="POST" action="/RECARGAS_13-06/public/recargamov" role="form">                

         {{ csrf_field() }}
         @if(count($errors) > 0)
         <div class="alert alert-danger">

           <ul>
            @foreach($errors->all() as $error)
            <li>{{ $error}} </li>
            @endforeach
          </ul>
        </div>
        @endif

        @if (session()->has('flash'))
        <div class="">
          <div class="alert alert-success">{{ session('flash')}}</div>


        </div>
        @endif


        
        <div class="form-group">
          <a href=""><img src="operadores/opemov.png"></a>

          <input type="hidden" name="idUsuario" value="21">
          <input type="hidden" name="idPago" value="1001">

          <input type="hidden" name=" idOperador" value="2">
        </div>


          <div class="form-group">
            <label for="">Valor a recargar</label>
            <input class="form-control"  name='valorRecarga' placeholder="$" type="number"  ue="{{ old('valorRecarga')}}" >
          </div>
          <div class="form-group">
            <label for="">Numero celular</label>
            <input class="form-control"  name='NumeroCelular' placeholder="Numero celular" type="number"  ue="{{ old('NumeroCelular')}}">
          </div>
          <div>
            <td>
              <button type="submit" class="btn btn-block btn-primary btn-sm">Realizar recarga</button>
            </td>
          </div>                 
        </form>  
      </div>
    </div>

     <div class="col-md-6">
          <div class="box box-solid">
             <!-- /.box-header -->
            <div class="box-body">
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
               
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="operadores/claroBaner1.jpeg" >

                   
                  </div>
                  <div class="item">
                    <img src="operadores/claroBaner2.jpeg" >
                   
                  </div>
                  
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                  <span class="fa fa-angle-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                  <span class="fa fa-angle-right"></span>
                </a>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Ultimas 5 recargas</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-6"></div><div class="col-sm-6"></div></div><div class="row"><div class="col-sm-12"><table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                <thead>
                <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Fecha</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Numero</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Costo</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Saldo</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">Estado</th></tr>
                </thead>
                <tbody>  <tr role="row" class="odd">
                  <td class="sorting_1">pendiente</td>
                  <td>pendiente</td>
                  <td>pendiente</td>
                  <td>pendiente</td>
                  <td>pendiente</td>
                
                </tfoot>
              </table></div></div><div class="row"><div class="col-sm-5"><div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div><div class="col-sm-7"><div class="dataTables_paginate paging_simple_numbers" id="example2_paginate"><ul class="pagination"><li class="paginate_button previous disabled" id="example2_previous"><a href="#" aria-controls="example2" data-dt-idx="0" tabindex="0">Previous</a></li><li class="paginate_button active"><a href="#" aria-controls="example2" data-dt-idx="1" tabindex="0">1</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="2" tabindex="0">2</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="3" tabindex="0">3</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="4" tabindex="0">4</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="5" tabindex="0">5</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="6" tabindex="0">6</a></li><li class="paginate_button next" id="example2_next"><a href="#" aria-controls="example2" data-dt-idx="7" tabindex="0">Next</a></li></ul></div></div></div></div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>



  




        @endsection
