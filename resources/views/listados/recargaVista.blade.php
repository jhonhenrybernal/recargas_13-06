@extends('layouts.app')

@section('content')

<section class="content-header">
      <h1>
        REPORTES
        <small>Recargas</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>
  
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
           <div class="box">
            <div class="box-header">
              <h3 class="box-title">Venta general</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <?php

              if( count($recargas) >0){
                          ?>


              <table id="tabla_recargas" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Fecha</th>
                  <th>Numero</th>
                  <th>Costo</th>
                  <th>Saldo</th>
                  <th>Estado</th>
                </tr>
                </thead>

                
                <tbody>

                  <?php

                foreach ($recargas as $recarga) {
                 # code...
                ?>
                <tr>
                  <td><?= $recarga->created_at; ?></td>
                  <td><?= $recarga->NumeroCelular; ?>
                  </td>
                  <td><?= $recarga->valorRecarga; ?></td>
                  <td>pendiente</td>
                  <td>pendiente</td>
                </tr>
                <?php
              }
              ?>
                
                </tbody>
               
              </table>

              <?php
              echo str_replace('/?', '?', $recargas->render());
            }
            else
            {
              ?>
            <div class='rechazado'><label style='color: #FA206A'>..NO HAY REGISTROS</label></div>

            <?php
          }
          ?>




            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

        @endsection
