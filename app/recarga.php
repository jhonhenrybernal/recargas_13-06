<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



class Recarga extends Model
{
    



   protected $table = 'recarga';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idUsuario', 'idOperador', 'NumeroCelular','idPago','valorRecarga',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'NumeroCelular', 'remember_token',
    ];
}
