<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operador extends Model
{
  

   protected $table = 'operador';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idOperador', 'nombreOperador', 'estadoOperador', 'idCupo','msjOperador',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
}
