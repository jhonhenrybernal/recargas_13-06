<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Operador;
use App\Precios;
use App\Pagos;


class vistaOpcionController extends Controller
{
   public function create(){
		$operadores = Operador::all();
		$precios = Precios::all();
		$pagos = Pagos::all();
		return view('recarga.browse', compact('operadores' , 'precios' , 'pagos'));

	}
}
