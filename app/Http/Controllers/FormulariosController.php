<?php namespace App\Http\Controllers;


use App\Operador;
use App\Recarga;
use App\Http\Requests;
use Illuminate\Http\Request;



class FormulariosController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function create(){
		$operadores = Operador::all();
		return view('recarga.browse', compact('operadores'));

	}


	//presenta el formulario para nueva recarga
	public function nuevaRecarga()
	{
		return view('recarga.browse');
		//return view('recarga.browse');
	}

	public function agregarRecarga(Request $request)
	{ 
		$this->validate($request,[
			'idUsuario' => 'required',
			'idOperador' => 'required',
			'NumeroCelular' => 'required',
			'valorRecarga' => 'required',
			'idPago' => 'required',


		]);
		Recarga::create($request->all());
						
		return redirect('/recarga')->with('flash', 'Hola! recarga exitosa');
	}




    public function nuevaRecargamov()
	{
		return view('recarga.movistar');
		//return view('recarga.browse');
	}

	public function agregarRecargamov(Request $request)
	{ 
		$this->validate($request,[
			'idUsuario' => 'required',
			'idOperador' => 'required',
			'NumeroCelular' => 'required',
			'valorRecarga' => 'required',
			'idPago' => 'required',


		]);
		Recarga::create($request->all());
						
		return redirect('/recargamov')->with('flash', 'Hola! recarga exitosa');
	}

	 public function nuevaRecargatig()
	{
		return view('recarga.tigo');
		//return view('recarga.browse');
	}

	public function agregarRecargatig(Request $request)
	{ 
		$this->validate($request,[
			'idUsuario' => 'required',
			'idOperador' => 'required',
			'NumeroCelular' => 'required',
			'valorRecarga' => 'required',
			'idPago' => 'required',


		]);
		Recarga::create($request->all());
						
		return redirect('/recargatig')->with('flash', 'Hola! recarga exitosa');
	}

	 public function nuevaRecargauf()
	{
		return view('recarga.uff');
		//return view('recarga.browse');
	}

	public function agregarRecargauf(Request $request)
	{ 
		$this->validate($request,[
			'idUsuario' => 'required',
			'idOperador' => 'required',
			'NumeroCelular' => 'required',
			'valorRecarga' => 'required',
			'idPago' => 'required',


		]);
		Recarga::create($request->all());
						
		return redirect('/recargauff')->with('flash', 'Hola! recarga exitosa');
	}

	public function nuevaRecargaex()
	{
		return view('recarga.exito');
		//return view('recarga.browse');
	}

	public function agregarRecargaex(Request $request)
	{ 
		$this->validate($request,[
			'idUsuario' => 'required',
			'idOperador' => 'required',
			'NumeroCelular' => 'required',
			'valorRecarga' => 'required',
			'idPago' => 'required',


		]);
		Recarga::create($request->all());
						
		return redirect('/recargaex')->with('flash', 'Hola! recarga exitosa');
	}

	public function nuevaRecargavir()
	{
		return view('recarga.virgi');
		//return view('recarga.browse');
	}

	public function agregarRecargavir(Request $request)
	{ 
		$this->validate($request,[
			'idUsuario' => 'required',
			'idOperador' => 'required',
			'NumeroCelular' => 'required',
			'valorRecarga' => 'required',
			'idPago' => 'required',


		]);
		Recarga::create($request->all());
						
		return redirect('/recargavir')->with('flash', 'Hola! recarga exitosa');
	}

	public function nuevaRecargaetb()
	{
		return view('recarga.etb');
		//return view('recarga.browse');
	}

	public function agregarRecargaetb(Request $request)
	{ 
		$this->validate($request,[
			'idUsuario' => 'required',
			'idOperador' => 'required',
			'NumeroCelular' => 'required',
			'valorRecarga' => 'required',
			'idPago' => 'required',


		]);
		Recarga::create($request->all());
						
		return redirect('/recargaetb')->with('flash', 'Hola! recarga exitosa');
	}


	public function nuevaRecargaava()
	{
		return view('recarga.avantel');
		//return view('recarga.browse');
	}

	public function agregarRecargaava(Request $request)
	{ 
		$this->validate($request,[
			'idUsuario' => 'required',
			'idOperador' => 'required',
			'NumeroCelular' => 'required',
			'valorRecarga' => 'required',
			'idPago' => 'required',


		]);
		Recarga::create($request->all());
						
		return redirect('/recargaava')->with('flash', 'Hola! recarga exitosa');
	}


	public function nuevaRecargaditv()
	{
		return view('recarga.directv');
		//return view('recarga.browse');
	}

	public function agregarRecargaditv(Request $request)
	{ 
		$this->validate($request,[
			'idUsuario' => 'required',
			'idOperador' => 'required',
			'NumeroCelular' => 'required',
			'valorRecarga' => 'required',
			'idPago' => 'required',


		]);
		Recarga::create($request->all());
						
		return redirect('/recargaditv')->with('flash', 'Hola! recarga exitosa');
	}


	public function nuevaRecargaun()
	{
		return view('recarga.une');
		//return view('recarga.browse');
	}

	public function agregarRecargaun(Request $request)
	{ 
		$this->validate($request,[
			'idUsuario' => 'required',
			'idOperador' => 'required',
			'NumeroCelular' => 'required',
			'valorRecarga' => 'required',
			'idPago' => 'required',


		]);
		Recarga::create($request->all());
						
		return redirect('/recargaun')->with('flash', 'Hola! recarga exitosa');
	}


	public function nuevaRecargaclaro()
	{
		return view('recarga.claro');
		//return view('recarga.browse');
	}

	public function agregarRecargaclaro(Request $request)
	{ 
		$this->validate($request,[
			'idUsuario' => 'required',
			'idOperador' => 'required',
			'NumeroCelular' => 'required',
			'valorRecarga' => 'required',
			'idPago' => 'required',


		]);
		Recarga::create($request->all());
						
		return redirect('/recargaclaro')->with('flash', 'Hola! recarga exitosa');
	}

	public function ListadoRecarga(){



		$recargas=Recarga::paginate();
	return view('listados.recargaVista')->with("recargas", $recargas);





	}

	public function ListadoRecargahome(){



		$recargas=Recarga::paginate(5);
	return view('home')->with("recargas", $recargas);





	}

		
	
}         