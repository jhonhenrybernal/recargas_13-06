<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Precios extends Model
{
   
   protected $table = 'precios';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idPrecio', 'Rango', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
}
